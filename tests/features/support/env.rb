#require 'capybara'
#require 'cucumber'
require 'capybara/cucumber'
require 'selenium-webdriver'    
#require 'capybara/dsl'

# putting this at the top level will make capybara methods available everywhere
# you can put it in a module if you want to, well, modularize ...
#include Capybara::DSL

Capybara.configure do |config|
    #selenium   selenium_chrome   selenium_chrome_healess
    Selenium::WebDriver::Chrome::Service.driver_path = 'C:\Windows\chromedriver.exe'
    config.default_driver = :selenium_chrome
    config.app_host = 'http://staging.pontaltech.com.br/login'    
    config.default_max_wait_time = 5
    end
    