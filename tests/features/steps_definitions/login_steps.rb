  Dado("que eu acesso a página principal") do
    visit 'http://staging.pontaltech.com.br/login'
  end
  
  Quando("eu faço o login com {string} e {string}") do |email, senha|
    login = LoginPage.new
    login.faz_login(email,senha)
   end
  
  Então("devo ver a seguinte mensagem {string}") do |mensagem|
    expect(page).to have_content mensagem
    sleep(4)
  end