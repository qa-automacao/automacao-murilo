class LoginPage
   
   include Capybara::DSL 

    def faz_login(email,senha)
        find('input[name=_username]').set email
        find('input[name=_password]').set senha
        click_on('Login')
    end    
end 