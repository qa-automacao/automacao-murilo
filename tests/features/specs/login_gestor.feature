    #language: pt

    Funcionalidade: Login 
    Para que eu possa logar na plataforma como gestor
    Podendo gerenciar as regras de bloqueio e usuários 
    O acesso será feito com um email e uma senha previamente cadastradas

    Contexto: Página inicial 
        Dado que eu acesso a página principal
    
    Esquema do Cenario: Tentativa de Login 
        Quando eu faço o login com "<email>" e "<senha>" 
        Então devo ver a seguinte mensagem "<alerta>"

    Exemplos: 
    |email|senha|alerta|
    #cenario inválido
    |gestor-b|q1w2e3|Usuário ou senha inválidos|  
    #cenario valido 
    |gestor-bv|q1w2e3|Gestor-adm| 








